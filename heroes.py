import requests
from bs4 import BeautifulSoup
import json
import re

heros = ["Abaddon",
"Alchemist",
"Ancient Apparition",
"Anti-Mage",
"Arc Warden",
"Axe",
"Bane",
"Batrider",
"Beastmaster",
"Bloodseeker",
"Bounty Hunter",
"Brewmaster",
"Bristleback",
"Broodmother",
"Centaur Warrunner",
"Chaos Knight",
"Chen",
"Clinkz",
"Clockwerk",
"Crystal Maiden",
"Dark Seer",
"Dark Willow",
"Dawnbreaker",
"Dazzle",
"Death Prophet",
"Disruptor",
"Doom",
"Dragon Knight",
"Drow Ranger",
"Earth Spirit",
"Earthshaker",
"Elder Titan",
"Ember Spirit",
"Enchantress",
"Enigma",
"Faceless Void",
"Grimstroke",
"Gyrocopter",
"Hoodwink",
"Huskar",
"Invoker",
"Io",
"Jakiro",
"Juggernaut",
"Keeper of the Light",
"Kunkka",
"Legion Commander",
"Leshrac",
"Lich",
"Lifestealer",
"Lina",
"Lion",
"Lone Druid",
"Luna",
"Lycan",
"Magnus",
"Mars",
"Medusa",
"Meepo",
"Mirana",
"Monkey King",
"Morphling",
"Naga Siren",
"Nature's Prophet",
"Necrophos",
"Night Stalker",
"Nyx Assassin",
"Ogre Magi",
"Omniknight",
"Oracle",
"Outworld Destroyer",
"Pangolier",
"Phantom Assassin",
"Phantom Lancer",
"Phoenix",
"Puck",
"Pudge",
"Pugna",
"Queen of Pain",
"Razor",
"Riki",
"Rubick",
"Sand King",
"Shadow Demon",
"Shadow Fiend",
"Shadow Shaman",
"Silencer",
"Skywrath Mage",
"Slardar",
"Slark",
"Snapfire",
"Sniper",
"Spectre",
"Spirit Breaker",
"Storm Spirit",
"Sven",
"Techies",
"Templar Assassin",
"Terrorblade",
"Tidehunter",
"Timbersaw",
"Tinker",
"Tiny",
"Treant Protector",
"Troll Warlord",
"Tusk",
"Underlord",
"Undying",
"Ursa",
"Vengeful Spirit",
"Venomancer",
"Viper",
"Visage",
"Void Spirit",
"Warlock",
"Weaver",
"Windranger",
"Winter Wyvern",
"Witch Doctor",
"Wraith King",
"Zeus"]

def create_hero_index_name(hero):
	hero_index = re.sub("[^a-zA-Z]", "", hero.lower())
	return hero_index

results = {}
for hero in heros:
	hero_link = "https://dota2.fandom.com/wiki/" + hero.replace(" ", "_") + "/Counters"
	html_text = requests.get(hero_link).text
	soup = BeautifulSoup(html_text, 'html.parser')
	elements = soup.select(".mw-parser-output h2 ~ div b a, .mw-parser-output h2")
	found_bad_against = False
	found_works_well_with = False
	hero_index = create_hero_index_name(hero)
	results[hero_index] = {"counters" : [], "pairs" : [], "display_name" : hero }
	for element in elements :
		if ( element.get_text() == "Bad against...[edit]" ):
			found_bad_against = True
			continue
		if ( element.get_text() == "Good against...[edit]"):
			found_bad_against = False
			continue
		if ( element.get_text() == "Works well with...[edit]" ):
			found_bad_against = False
			found_works_well_with = True
			continue
		if ( found_bad_against == True ):
			results[hero_index]["counters"].append(create_hero_index_name(element.get_text()))
		elif ( found_works_well_with == True) :
			results[hero_index]["pairs"].append(create_hero_index_name(element.get_text()))

json = json.dumps(results)
print(json)
f = open("heros.json", "w")
f.write(json)
f.close()